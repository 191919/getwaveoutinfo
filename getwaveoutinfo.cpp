/**********************************************************************

GetWaveOutInfo.cpp
A reverse-engineering work to get WAVEFORMATEX from a HWAVEOUT object

Copyright (c) 2012 jh <jh@6.cn>.
Source code distributed under GPLv3 license.

Tested under:

- Windows XP SP2
- Windows XP SP3
- Windows Vista
- Windows Vista SP1
- Windows 7
- Windows 7 SP1

Limitations:

1. 8bit+2channels cannot be distinguished from 16bit+1channel.
2. Code is ugly, extremely ugly.

**********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <windows.h>
#include <mmsystem.h>

#pragma comment(lib, "winmm.lib")
#pragma comment(lib, "user32.lib")

void dump_mem(BYTE* buf, int len)
{
    for (int i = 0; i < len; ++i)
    {
        printf("%02x ", buf[i]);
        if (i % 16 == 15) printf("\n");
    }
}

LPCWAVEFORMATEX waveOutGetInfo(HWAVEOUT wo, UINT* puDeviceId)
{
    typedef struct
    {
        BYTE bUnk[64];
        LPWAVEFORMATEX pwfx;
    } __gf_002;
    typedef struct
    {
        BYTE bUnk1[32];

        DWORD dwUnk2;
        DWORD dwUnk3;
        DWORD nBlockAlign;
        DWORD dwUnk4;

        DWORD nAvgBytesPerSec;
        DWORD dwUnk5;
        DWORD nSamplesPerSec;
        DWORD dwUnk6;
    } __gf_003;
    typedef struct
    {
        LPVOID* pUnk1;
        DWORD dwUnk2;
        union
        {
            __gf_002* p_gf_002;
            __gf_003* p_gf_003;
        };
        UINT uDeviceId;
    } __gf_001;

    __gf_001* p_gf_001 = (__gf_001*) wo;
    *puDeviceId = p_gf_001->uDeviceId;

    if (p_gf_001->uDeviceId != WAVE_MAPPER)
    {
        LPWAVEFORMATEX pwfx = (LPWAVEFORMATEX) calloc(1, sizeof(WAVEFORMATEX));
        pwfx->wFormatTag = WAVE_FORMAT_PCM;
        pwfx->wBitsPerSample = 16;
        pwfx->nChannels = p_gf_001->p_gf_003->nBlockAlign / (pwfx->wBitsPerSample / 8);
        pwfx->nSamplesPerSec = p_gf_001->p_gf_003->nSamplesPerSec;
        pwfx->nAvgBytesPerSec = p_gf_001->p_gf_003->nAvgBytesPerSec;
        pwfx->nBlockAlign = pwfx->nChannels * pwfx->wBitsPerSample / 8;
        pwfx->cbSize = sizeof(*pwfx);
        return pwfx;
    }

    LPWAVEFORMATEX pwfx = (LPWAVEFORMATEX) calloc(1, sizeof(WAVEFORMATEX));
    memcpy(pwfx, p_gf_001->p_gf_002->pwfx, sizeof(*pwfx));
    pwfx->cbSize = sizeof(*pwfx);
    return pwfx;
}

int main()
{
    UINT uNumOfDevs = waveOutGetNumDevs();
    for (UINT i = -1; (int) i < (int) uNumOfDevs; ++i)
    {
        WAVEOUTCAPS woc = { 0 };
        waveOutGetDevCaps(i, &woc, sizeof(woc));
        printf("Device: %s\n", woc.szPname);

        HWAVEOUT wo = NULL;
        WAVEFORMATEX wfx = { 0 };

#define SAMPLES 32000

        wfx.wFormatTag = WAVE_FORMAT_PCM;
        wfx.nChannels = 1;
        wfx.nSamplesPerSec = SAMPLES;
        wfx.wBitsPerSample = 16;
        wfx.nBlockAlign = wfx.nChannels * wfx.wBitsPerSample / 8;
        wfx.nAvgBytesPerSec = SAMPLES * wfx.nChannels * wfx.wBitsPerSample / 8;
        wfx.cbSize = sizeof(wfx);

        MMRESULT res = 0;
        res = waveOutOpen(&wo, i, &wfx, 0, 0, CALLBACK_EVENT);

        if (wo == NULL)
        {
            printf("shit: %d\n", res);
            continue;
        }

        printf("wfx=%08p wo=%08p\n", &wfx, wo);

        UINT uDeviceId = 0;
        LPCWAVEFORMATEX pwfx = waveOutGetInfo(wo, &uDeviceId);
        if (pwfx != NULL)
        {
            printf(
                "nChannels\t\t%d\n"
                "nSamplesPerSec\t\t%d\n"
                "wBitsPerSample\t\t%d\n",
                pwfx->nChannels, pwfx->nSamplesPerSec, pwfx->wBitsPerSample
            );
        }
        printf("DeviceId = %d\n-----------------\n", uDeviceId);
    }

	return 0;
}